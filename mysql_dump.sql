-- --------------------------------------------------------
-- Сервер:                       127.0.0.1
-- Версія сервера:               10.1.15-MariaDB - mariadb.org binary distribution
-- ОС сервера:                   Win64
-- HeidiSQL Версія:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for api
CREATE DATABASE IF NOT EXISTS `api` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `api`;


-- Dumping structure for таблиця api.friends_request
CREATE TABLE IF NOT EXISTS `friends_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_user` int(11) NOT NULL DEFAULT '0',
  `from_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to_user` (`to_user`),
  KEY `from_user` (`from_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table api.friends_request: ~0 rows (приблизно)
/*!40000 ALTER TABLE `friends_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `friends_request` ENABLE KEYS */;


-- Dumping structure for таблиця api.friend_relation
CREATE TABLE IF NOT EXISTS `friend_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one` int(11) NOT NULL,
  `user_two` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `relation` (`user_one`,`user_two`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table api.friend_relation: ~0 rows (приблизно)
/*!40000 ALTER TABLE `friend_relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `friend_relation` ENABLE KEYS */;


-- Dumping structure for таблиця api.token
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `expiry_time` int(11) DEFAULT NULL,
  `sid` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table api.token: ~0 rows (приблизно)
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
REPLACE INTO `token` (`id`, `user_id`, `expiry_time`, `sid`) VALUES
	(1, 1, 1469985178, 'OAUFhDYTWInPjyFNNxrOkGVLtFYgVcvwqTNqothKTuKNECvztz');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;


-- Dumping structure for таблиця api.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '0',
  `surname` varchar(50) DEFAULT '0',
  `nickname` varchar(50) DEFAULT '0',
  `registered` int(11) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `textual_info` (`name`,`surname`,`nickname`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table api.user: ~0 rows (приблизно)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `name`, `surname`, `nickname`, `registered`, `password`) VALUES
	(1, 'test', 'test', 'test', 0, '$2a$04$ifIDX3oW117E4RvHxotKgOKdNuKm.l/BSe..IfH0ZPpFcLZ.opXTO');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
