<?php

use Phalcon\Di;
use Phalcon\Mvc\Application;

ini_set('display_errors', 1);
error_reporting(E_ALL);

define('APP_PATH', realpath('..'));

try {
    $config = require APP_PATH . "/app/config/config.php";

    date_default_timezone_get($config->timezone);

    include APP_PATH . "/vendor/autoload.php";
    include APP_PATH . "/app/config/loader.php";
    include APP_PATH . "/app/config/services.php";

    $application = new Application($di);

    echo $application->handle()->getContent();
} catch (Exception $e) {

    if ($config->environment->name === 'developer') {
        echo $e->getMessage() . '<br>';
        echo '<pre>' . $e->getTraceAsString() . '</pre>';
    } else {
        echo "Something went wrong";
    }
}
