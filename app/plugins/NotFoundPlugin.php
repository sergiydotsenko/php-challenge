<?php

namespace App\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatcherException;
use Phalcon\Mvc\User\Plugin;
use Exception;

class NotFoundPlugin extends Plugin {

    public function beforeException(Event $event, MvcDispatcher $dispatcher, Exception $exception) {
        
        if (!($exception instanceof DispatcherException)) {
            // log everything except dispatcher errors (404,..)
            $this->di->get('logger')->error($exception->getMessage() . PHP_EOL . $exception->getTraceAsString());
        }
        $this->view->disable();

        $dispatcher->forward(array(
            'namespace' => $this->di->get('config')->environment->namespace . '\\Controllers',
            'controller' => 'index',
            'action' => 'error'
        ));
        return false;
    }

}
