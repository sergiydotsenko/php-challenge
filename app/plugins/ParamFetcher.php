<?php

namespace App\Plugins;

use Closure;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Validation\Exception;

class ParamFetcher extends Plugin {

    private $params = [];
    private $validators = [];
    private $fields = [];
    private $rawData = [];
    private $filterProps = [];
    private $request;

    public function __construct() {
        $this->request = \Phalcon\Di::getDefault()->get('request');
    }

    public function __get($paramName) {
        if (isset($this->params[$paramName])) {
            return $this->params[$paramName];
        } else {
            throw new Exception('No such param in fetched params `' . $paramName . '`');
        }
    }

    public function fetch($list = [], $type = '') {


        $list = array_merge($this->fields,$list);

        $this->filterProps = $list;
        $error = [];

        $this->iterateFields($list, $type, $error);

        $this->params = $this->rawData;
        return $this->rawData;
    }

    /**
     * 
     * @return array
     */
    public function all() {
        return $this->params;
    }

    public function addFetchArgument(array $opts) {
        $this->fields = array_merge($this->fields, $opts);
    }

    public function addValidationRule($name, Closure $f, $message = '', $code = 0) {
        $this->validators[$name] = ['validator' => $f, 'message' => $message, 'code' => $code];
    }

    public function validateVariable(&$var, $filters, &$error, $fieldName = '', $default = null) {

        $notRequired = in_array('?', $filters);
        foreach ($filters as $filter) {
            $disjunktive = explode('|', $filter);
            if (count($disjunktive) === 1) {
                $fResult = $this->applyFilter($var, $filter, $error, $fieldName);
                if (!$fResult && !$notRequired) {
                    $error = ['Params validation failed', $fieldName, $error];
                    return false;
                } elseif (!$fResult && $notRequired) {
                    $var = $default;
                    break;
                };
            } else {
                $onePositive = false;

                foreach ($disjunktive as $disjunktiveFilter) {
                    if ($this->applyFilter($var, $disjunktiveFilter, $error, $fieldName)) {
                        $onePositive = true;
                        break;
                    }
                }
                if (!$onePositive) {
                    if (!$notRequired) {

                        $error = ['Params validation failed', $fieldName, $error];

                        return false;
                    } elseif ($notRequired) {
                        $var = $default;
                        break;
                    };
                }
            }
        }
        return true;
    }

    public function iterateFields(array $list, $type = '', &$error) {
        foreach ($list as $item => $default) {
            $fDef = $default;
            $filters = false;
            $methodName = 'get' . $type;
            if (gettype($item) !== 'string') {
                $item = $default;
                $default = null;
            } else {
                if (is_array($default) && isset($default['filter'])) {
                    $filters = $default['filter'];
                    unset($default['filter']);
                    if (isset($default['def'])) {
                        $default = $default['def'];
                    } else {
                        $default = '';
                    }
                }
            }

            $explodeFieldNames = explode(',', $item);

            if (count($explodeFieldNames) > 1) {
                $mergedFilters = [];
                foreach ($explodeFieldNames as $efn) {
                    $mergedFilters[$efn] = $fDef;
                }

                $this->iterateFields($mergedFilters, $type, $error);
            } else {
                $this->rawData[$item] = $this->request->$methodName($item, null, $default);
                if ($filters) {
                    if (!$this->validateVariable($this->rawData[$item], $filters, $error, $item, $default)) {
                        throw new \Exception($error[0] . ': ' . $error[1] . ' ' . $error[2][1]);
                    };
                }
            }
        }
        $data[$item] = $this->request->$methodName($item, null, $default);
    }

    public function applyFilter(&$variable, $filter, &$error = null, $fieldName = null) {

        $firstLetter = substr($filter, 0, 1);
        if (in_array($firstLetter, ['#', '&', '>', '=', '<', '*', '!', '@', '^', '[', '{', '~'])) {
            $valToCompare = substr($filter, 1);

            switch ($firstLetter) {

                case '~':;
                    $validatorName = substr($filter, 1);
                    if (!isset($this->validators[$validatorName])) {
                        throw new \Exception('No validator `' . $validatorName . '` found');
                    } else {
                        $error = [$this->validators[$validatorName]['code'], $this->validators[$validatorName]['message']];
                        return $this->validators[$validatorName]['validator']($variable);
                    }
                    break;

                case '>' :
                    if (is_integer($variable)) {
                        if ($variable <= intval($valToCompare)) {
                            $error = [10, 'should be greater than ' . $valToCompare];
                            return false;
                        }
                    }
                    if (is_string($variable)) {
                        if (strlen($variable) <= intval($valToCompare)) {
                            $error = [11, 'length should be greater than ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_float($variable)) {
                        if (floatval($variable) <= floatval($valToCompare)) {
                            $error = [12, 'value should be greater than ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_array($variable)) {
                        if (count($variable) <= intval($valToCompare)) {
                            $error = [13, 'array should contain at least ' . $valToCompare . ' elements'];
                            return false;
                        }
                    }

                    break;
                case '<':

                    if (is_integer($variable)) {
                        if ($variable >= intval($valToCompare)) {
                            $error = [20, 'value should be less than' . $valToCompare];
                            return false;
                        }
                    }
                    if (is_string($variable)) {
                        if (strlen($variable) >= intval($valToCompare)) {
                            $error = [21, 'value length should be less than ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_float($variable)) {
                        if (floatval($variable) >= floatval($valToCompare)) {
                            $error = [22, 'value should be less than ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_array($variable)) {
                        if (count($variable) >= intval($valToCompare)) {
                            $error = [23, 'array should contain less than ' . $valToCompare . ' elements'];
                            return false;
                        }
                    }

                    break;
                case '=':

                    if (is_integer($variable)) {
                        if (intval($variable) !== intval($valToCompare)) {
                            $error = [30, 'value should be equal to ' . $valToCompare];
                            return false;
                        }
                    }
                    if (is_string($variable)) {
                        if (strval($variable) !== strval($valToCompare)) {
                            $error = [31, 'value should be equal to ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_float($variable)) {
                        if (floatval($variable) !== floatval($valToCompare)) {
                            $error = [32, 'value should be equal to ' . $valToCompare];
                            return false;
                        }
                    }

                    if (is_array($variable)) {
                        if (count($variable) === intval($valToCompare)) {
                            $error = [33, 'array should contain ' . $valToCompare . ' elements'];
                            return false;
                        }
                    }

                    break;
                case '*':
                    if (!preg_match($valToCompare, $variable)) {
                        $error = [40, 'bad format'];
                        return false;
                    }
                    break;
                case '@':
                    $params = explode(',', $valToCompare);
                    if (!in_array($variable, $params)) {
                        $error = [50, 'should be one of "' . implode(',', $params) . '"'];
                        return false;
                    }
                    break;
                case '{':
                    if (is_array($variable)) {
                        $props = explode(',', substr($valToCompare, 0, -1));
                        foreach ($props as $prop) {
                            if (!isset($variable[$prop])) {
                                $error = [90, 'should conntain attribute \'' . $prop . '\''];
                                return false;
                            }
                        }
                    } else {
                        $error = [80, 'should be array'];
                        return false;
                    }
                    break;
                case '^':
                    $params = explode(',', $valToCompare);
                    if (in_array($variable, $params)) {
                        $error = [60, 'should not be one of "' . implode(',', $params) . '"'];
                        return false;
                    }
                    break;
                case '!':
                    if (empty($variable)) {
                        $error = [70, 'required but empty'];
                        return false;
                    }
                    break;
                case '[':
                    if ($valToCompare[strlen($valToCompare) - 1] === ']') {
                        if (!is_array($variable)) {
                            $error = [80, 'should be array'];
                            return false;
                        } else {
                            $arrayElementsType = explode(']', $valToCompare)[0];
                            if ($arrayElementsType === '*') {
                                return true;
                            } else {
                                foreach ($variable as $vItem) {
                                    if (!$this->typeFilter($vItem, $arrayElementsType)) {
                                        $error = [81, 'should be array of ' . $arrayElementsType];
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    break;
                default:
                    break;
            }
        } else {
            return $this->typeFilter($variable, $filter, $error);
        }

        return true;
    }

    private function typeFilter(&$variable, $filter, &$error) {

        $valToCompare = substr($filter, 1);
        $filter = substr($filter, 0, 1);

        switch ($filter) {
            case 'a':
                if (!is_array($variable)) {
                    $error = [90, 'should be array'];
                    return false;
                }
                break;
            case 'i':
                $intval = intval($variable);

                if (strval($intval) !== strval($variable)) {
                    $error = [100, 'should be int'];
                    return false;
                } else {
                    $variable = $intval;
                }
                break;
            case 'f':

                $float = floatval($variable);

                if (!is_float($float) || strval($float) !== strval($variable)) {
                    $error = [110, 'should be float'];
                    return false;
                } else {
                    $variable = $float;
                }
                break;
            case 's':

                //echo 'consider to (' . print_r($variable, 1) . ') to be string '.strlen($variable).' <br/>';

                if (!is_string($variable) || strlen($variable) <= 0) {
                    $error = [120, 'should be string'];
                    return false;
                }
                break;
            case 'e':
                if (!filter_var($variable, FILTER_VALIDATE_EMAIL)) {
                    $error = [130, 'should be valid email'];
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

}
