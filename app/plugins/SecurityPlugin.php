<?php

namespace App\Plugins;

use App\Models\Token;
use App\Models\User;
use Phalcon\Text;

class SecurityPlugin {

    const DEFAULT_EXPIRE_IN = 60 * 60 * 24;

    /**
     *
     * @var \App\Models\User;
     */
    private $user = null;

    /**
     * 
     * @param User $u
     * @param type $lifetime
     * @return  App\Models\Token
     */
    public function generateToken(User $u, $lifetime = self::DEFAULT_EXPIRE_IN) {

        $token = new Token();
        $token->expiry_time = time() + $lifetime;
        $token->sid = Text::random(Text::RANDOM_ALPHA, 50);
        $token->user_id = $u->id;
        $token->save();

        return $token;
    }

    /**
     * 
     * @param User $u
     * @return  App\Models\Token
     */
    public function getValidToken(User $u) {
        $validToken = Token::findFirst(
                        [
                            'conditions' => 'expiry_time > ?0 AND user_id = ?1',
                            'bind' => [
                                time(),
                                $u->id
                            ]
        ]);
        if (!$validToken) {
            return $this->generateToken($u);
        } else {
            return $validToken;
        }
    }

    /**
     * 
     * @param type $token
     * @return bool | App\Models\Token
     */
    public function isTokenValid($token) {

        $token = Token::findFirstBySid($token);
        if (!$token) {
            return false;
        } else {
            // that's bad, but
            
            $this->user = $token->User;
            return $token;
        }
    }

    /**
     * 
     * @return \App\Models\User
     */
    public function getLastValidatedUser() {
        return $this->user;
    }

    /**
     * 
     * @param type $login
     * @param type $password
     * @return App\Models\User | bool
     */
    public function checkUserLogin($login, $password) {
        $user = User::findFirstByNickname($login);
        if (!empty($user)) {
            if (password_verify($password, $user->password)) {
                $this->user = $user;
                return $user;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
