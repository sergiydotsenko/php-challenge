<?php

namespace App\Plugins;

abstract class SearchEngine {

    /**
     * @return \App\Models\User[] 
     */
    abstract function find($criteria, array $fields = []);
}
