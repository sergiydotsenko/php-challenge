<?php

namespace App\Plugins\SearchEngine;

use App\Models\User;
use App\Plugins\SearchEngine;

class MysqlSearchEngine extends SearchEngine {

    /**
     * 
     * @param type $criteria
     * @return User[]
     */
    public function find($criteria, array $columns = []) {

        $options = [
            'conditions' => 'name LIKE "%'.$criteria.'%" OR surname LIKE "%'.$criteria.'%" OR nickname LIKE "%'.$criteria.'%"'
        ];

        if (!empty($columns)) {
            $options['columns'] = implode(',', $columns);
        }

        return User::find($options);
    }

}
