<?php

use App\Plugins\NotFoundPlugin;
use App\Plugins\ParamFetcher;
use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger\Adapter\File;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;

$di = new FactoryDefault();

$di->setShared('config', $config);

$di->setShared('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

$di->setShared('router', function () {
    $router = require_once APP_PATH . '/app/config/routes.php';
    return $router;
});


$di->setShared('db', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

$di->setShared('logger', function() use ($config) {
    $adapter = new File($config->logfile);
    return $adapter;
});



$di->setShared('dispatcher', function() {
    $dispatcher = new Dispatcher();
    $dispatcher->setDefaultNamespace('App\\Controllers');



    $eventsManager = new EventsManager();

    $eventsManager->attach('dispatch:beforeException', new NotFoundPlugin());

    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

$di->setShared('paramFetcher', function() {
    return new ParamFetcher();
});


$di->setShared('security', function() {
    return new App\Plugins\SecurityPlugin();
});

$di->setShared('searchEngine',function(){
   return new App\Plugins\SearchEngine\MysqlSearchEngine(); 
});

$di->setShared('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ));

            return $volt;
        },
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
            ));

            return $view;
        });


        