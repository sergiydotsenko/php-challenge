<?php

use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs(
        array(
            $config->application->controllersDir,
            $config->application->modelsDir,
            $config->application->pluginsDir,
            $config->application->traitsDir
        )
)->register();

$loader->registerNamespaces([
    'App\Models' => $config->application->modelsDir,
    'App\Controllers' => $config->application->controllersDir,
    'App\Plugins' => $config->application->pluginsDir,
    'App\Traits' => $config->application->traitsDir
]);