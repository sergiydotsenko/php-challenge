<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

return new \Phalcon\Config(array(
    'environment' => [
        'name' => 'developer',
        'mapping' => [
            'public' => APP_PATH . '/public/'
        ],
        'namespace' => 'App'
    ],
    'database' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'root',
        'dbname' => 'api',
        'charset' => 'utf8',
    ),
    'application' => array(
        'rootDir' => APP_PATH,
        'applicationDir' => APP_PATH . '/app/',
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir' => APP_PATH . '/app/models/',
        'migrationsDir' => APP_PATH . '/app/migrations/',
        'viewsDir' => APP_PATH . '/app/views/',
        'pluginsDir' => APP_PATH . '/app/plugins/',
        'libraryDir' => APP_PATH . '/app/library/',
        'cacheDir' => APP_PATH . '/app/cache/',
        'traitsDir' => APP_PATH . '/app/traits/',
        'baseUri' => '/',
    ),
    'logfile' => APP_PATH . '/error.log',
    'timezone' => 'Europe/Kiev',
    'memcache' => [
        'port' => 11211,
        'host' => 'localhost',
        'persistent' => false
    ],
    'cookie' => [
        'key' => 'NodqVQcGMnz5HaqvH6x'
    ]
        ));
