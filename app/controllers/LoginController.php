<?php

namespace App\Controllers;

class LoginController extends BaseController {

    public function indexAction() {
        try {
            $this->params->fetch([
                'login' => ['filter' => ['!', 'string', '>0']],
                'password' => ['filter' => ['!', 'string', '>0']]
            ]);

            $user = $this->security->checkUserLogin($this->params->login, $this->params->password);

            if ($user) {
                return $this->apiResult(['token' => $this->security->getValidToken($user)->sid]);
            } else {
                return $this->failApiResult(['message' => 'wrong login or password']);
            }
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

}
