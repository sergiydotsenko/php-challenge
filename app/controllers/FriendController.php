<?php

namespace App\Controllers;

use App\Models\FriendRelation;
use App\Models\FriendsRequest;

class FriendController extends BaseController {

    public static $defaultVisibleFields = ['id', 'name', 'surname', 'registered'];

    public function onConstruct() {

        parent::onConstruct();

        // this adds required token for each action in this controller
        $this->params->addFetchArgument([
            'token' => ['filter' => ['!', '~validToken']]
        ]);
    }

    public function acceptAction() {
        try {
            $this->params->fetch([
                'user_id' => ['filter' => ['!', 'int', '~userExists']]
            ]);

            $me = $this->security->getLastValidatedUser();

            $checkRequest = FriendsRequest::findFirst([
                        'conditions' => 'from_user = ?0 AND to_user = ?1',
                        'bind' => [
                            $this->params->user_id,
                            $me->id
                        ]
            ]);

            if ($checkRequest) {

                $friendshipRelation = new FriendRelation();
                $friendshipRelation->save([
                    'user_one' => $me,
                    'user_two' => $this->params->user_id
                ]);

                $checkRequest->delete();

                return $this->apiResult();
            } else {
                return $this->failApiResult([
                            'message' => 'no friend request from user you provided'
                ]);
            }
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function addAction() {
        try {
            $this->params->fetch([
                'user_id' => ['filter' => ['!', 'int', '~userExists']]
            ]);

            $me = $this->security->getLastValidatedUser();

            if (FriendRelation::exists($me->id, $this->params->user_id)) {
                return $this->failApiResult([
                            'message' => 'already in a friendship'
                ]);
            } else {

                $checkRequest = FriendsRequest::findFirst([
                            'conditions' => 'from_user = ?0 AND to_user = ?1',
                            'bind' => [
                                $me->id,
                                $this->params->user_id
                            ]
                ]);

                if ($checkRequest) {
                    return $this->failApiResult([
                                'message' => 'already in a friendship'
                    ]);
                } else {

                    $newRequest = new FriendsRequest();
                    $newRequest->save([
                        'from_user' => $me->id,
                        'to_user' => $this->params->user_id
                    ]);

                    return $this->apiResult();
                }
            }
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function allAction() {
        try {
            $this->params->fetch();

            $result = ['users' => []];

            $friends = FriendRelation::allOf($this->security->getLastValidatedUser(), self::$defaultVisibleFields);

            if ($friends) {
                $result['users'] = $friends->toArray();
            }

            return $this->apiResult($result);
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function declineAction() {
        try {
            $this->params->fetch([
                'user_id' => ['filter' => ['!', 'int', '~userExists']]
            ]);

            $me = $this->security->getLastValidatedUser();

            $checkRequest = FriendsRequest::findFirst([
                        'conditions' => 'from_user = ?0 AND to_user = ?1',
                        'bind' => [
                            $this->params->user_id,
                            $me->id
                        ]
            ]);

            if ($checkRequest) {
                $checkRequest->delete();

                return $this->apiResult();
            } else {
                return $this->failApiResult([
                            'message' => 'no friend request from user you provided'
                ]);
            }
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function inboxAction() {
        try {
            $this->params->fetch();

            $result = ['users' => []];

            $me = $this->security->getLastValidatedUser();

            $requests = FriendsRequest::find([
                        'conditions' => 'to_user = ?0',
                        'bind' => [
                            $me->id
                        ]
            ]);

            // here can be used sql join, but i dont want to do this riht now
            foreach ($requests as $req) {
                $result['users'][] = \App\Models\User::findFirst($req->from_user)->toArray(self::$defaultVisibleFields);
            }

            return $this->apiResult($result);
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function outboxAction() {
        try {

            $this->params->fetch();

            $result = ['users' => []];

            $me = $this->security->getLastValidatedUser();

            $requests = FriendsRequest::find([
                        'conditions' => 'from_user = ?0',
                        'bind' => [
                            $me->id
                        ]
            ]);

            // here can be used sql join, but i dont want to do this riht now

            foreach ($requests as $req) {
                $result['users'][] = \App\Models\User::findFirst($req->to_user)->toArray(self::$defaultVisibleFields);
            }

            return $this->apiResult($result);
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

    public function removeAction() {
        try {
            $this->params->fetch([
                'user_id' => ['filter' => ['!', 'int', '~userExists']]
            ]);

            $me = $this->security->getLastValidatedUser();

            $friendship = FriendRelation::exists($me->id, $this->params->user_id);

            if (!$friendship) {
                return $this->failApiResult([
                            'message' => 'not in a friendship'
                ]);
            } else {
                $friendship->delete();
                return $this->apiResult();
            }
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

}
