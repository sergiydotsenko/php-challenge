<?php

namespace App\Controllers;

class ProfileController extends BaseController {

    public function onConstruct() {

        parent::onConstruct();

        $this->params->addFetchArgument(['token' => ['filter' => ['!', '~validToken']]]);
    }

    public function indexAction() {

        try {
            $this->params->fetch();

            $user = $this->security->getLastValidatedUser();

            return $this->apiResult([
                        'user' => $user->toArray(FriendController::$defaultVisibleFields)
            ]);
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

}
