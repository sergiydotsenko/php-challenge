<?php

namespace App\Controllers;

class SearchController extends BaseController {

    // token is not obligated i guess
    public function indexAction() {
        try {
            $this->params->fetch([
                'criteria' => ['filter' => ['!', 'string', '>0']]
            ]);

            $result = ['users' => []];

            $searchEngine = $this->di->get('searchEngine');
            $found = $searchEngine->find(addslashes($this->params->criteria), ['id', 'name', 'surname', 'registered']);

            if ($found->count()) {
                $result['users'] = $found->toArray();
            }

            return $this->apiResult($result);
        } catch (\Exception $e) {
            return $this->failApiResult([
                        'message' => $e->getMessage()
            ]);
        }
    }

}
