<?php

namespace App\Controllers;

class IndexController extends BaseController {

    public function errorAction() {

        $this->response->setStatusCode(404);
        return $this->apiResult(['message' => 'not found']);
    }

}
