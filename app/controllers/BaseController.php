<?php

namespace App\Controllers;

use App\Models\User;
use App\Plugins\ParamFetcher;
use App\Plugins\SecurityPlugin;
use Phalcon\Mvc\Controller;

class BaseController extends Controller {

    /**
     *
     * @var ParamFetcher
     */
    protected $params;

    /**
     *
     * @var SecurityPlugin
     */
    protected $security;

    public function onConstruct() {

        session_write_close();

        $this->params = $this->di->get('paramFetcher');
        $this->security = $security = $this->di->get('security');

        /* @var  $security SecurityPlugin */
        $this->params->addValidationRule('validToken', function($val) use (&$security) {
            if ($security->isTokenValid($val)) {
                return true;
            } else {
                return false;
            }
        },' is not valid',1);
        
        $this->params->addValidationRule('userExists', function($id){
            return User::findFirstById($id);
        },'user not exists',2);
        
    }

    public function apiResult($data = []) {
        return $this->response->setJsonContent((object) $data);
    }

    public function failApiResult($data) {
        return $this->apiResult(array_merge(['state' => false], $data));
    }

}
