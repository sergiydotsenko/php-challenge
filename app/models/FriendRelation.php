<?php

namespace App\Models;

class FriendRelation extends BaseModel {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_one;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_two;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'friend_relation';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return FriendRelation[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return FriendRelation
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    public static function allOf(User $user, $columns = []) {
        $options = [
            'conditions' => 'user_one = ?0 AND user_two = ?0',
            'bind' => [
                $user->id
            ]
        ];

        if (!empty($columns)) {
            $options['columns'] = implode(',', $columns);
        }

        return self::findFirst($options);
    }

    /**
     * 
     * @param type $userOne
     * @param type $userTwo
     * @return bool
     */
    public static function exists($userOne, $userTwo) {
        return self::findFirst([
                    'conditions' => '(user_one = ?0 AND user_two = ?1) OR (user_two = ?0 AND user_one = ?1)',
                    'bind' => [
                        $userOne, $userTwo
                    ]
        ]);
    }

}
