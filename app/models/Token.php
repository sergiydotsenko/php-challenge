<?php

namespace App\Models;

/**
 * @method \App\Models\User findFirstBySid(string $sid)
 * @property \App\Models\User $User 
 */
class Token extends BaseModel {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    public $expiry_time;

    /**
     *
     * @var string
     * @Column(type="string", length=150, nullable=true)
     */
    public $sid;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'token';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Token[]
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Token
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    public function initialize() {
        $this->hasOne('user_id', '\App\Models\User', 'id', [
            'alias' => 'User'
        ]);
    }

}
